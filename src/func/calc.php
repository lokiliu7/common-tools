<?php

/**
 * ------------------------------------------------------------------------------------------------------------
 * 计算、换算
 * ------------------------------------------------------------------------------------------------------------
 */

if (!function_exists('calc_add')) {
    /**
     * 加法计算
     *
     * @param string $num1
     * @param string $num2
     * @param null $scale
     * @return string
     */
    function calc_add(string $num1, string $num2, $scale = null): string
    {
        // 取最大的小数位
        if (is_null($scale)) {
            $scale = [decimal_places($num1), decimal_places($num2)];
            rsort($scale);
            $scale = current($scale);
        }

        return bcadd($num1, $num2, $scale);
    }
}

if (!function_exists('calc_sub')) {
    /**
     * 减法计算
     *
     * @param string $num1
     * @param string $num2
     * @param null $scale
     * @return string
     */
    function calc_sub(string $num1, string $num2, $scale = null): string
    {
        // 取最大的小数位
        if (is_null($scale)) {
            $scale = [decimal_places($num1), decimal_places($num2)];
            rsort($scale);
            $scale = current($scale);
        }

        return bcsub($num1, $num2, $scale);
    }
}

if (!function_exists('calc_mul')) {
    /**
     * 乘法计算
     *
     * @param string $num1
     * @param string $num2
     * @param null $scale
     * @return string
     */
    function calc_mul(string $num1, string $num2, $scale = null): string
    {
        // 取两值小数位和
        if (is_null($scale)) {
            $scale = decimal_places($num1) + decimal_places($num2);
        }

        return bcmul($num1, $num2, $scale);
    }
}

if (!function_exists('calc_div')) {
    /**
     * 除法计算
     *
     * @param string $num1
     * @param string $num2
     * @param null $scale
     * @return string
     */
    function calc_div(string $num1, string $num2, $scale = null): string
    {
        // 小数位未设置默认取15
        return bcdiv($num1, $num2, $scale ?? 15);
    }
}

if (!function_exists('calc_pow')) {
    /**
     * 乘方计算
     *
     * @param string $num1
     * @param $num2
     * @param null $scale
     * @return string
     */
    function calc_pow(string $num1, $num2, $scale = null): string
    {
        // 值小数位 乘 次方
        if (is_null($scale)) {
            $scale = decimal_places($num1) * $num2;
        }

        return bcpow($num1, $num2, $scale);
    }
}

if (!function_exists('number_string')) {
    /**
     * 数字 转换成字符串格式
     *
     * @param $number
     * @return string
     */
    function number_string($number): string
    {
        return int_string($number);
    }
}

if (!function_exists('int_string')) {
    /**
     * 整数 转换成字符串格式
     *
     * @param $number
     * @return string
     */
    function int_string($number): string
    {
        if (decimal_places($number)) {
            // 浮点数
            return float_string($number);
        }

        // 整数
        return sprintf('%s', $number);
    }
}

if (!function_exists('float_string')) {
    /**
     * 浮点数 转换成字符串格式
     *
     * @param $number
     * @return string
     */
    function float_string($number): string
    {
        if ($len = decimal_places($number)) {
            // 浮点数
            return sprintf('%.' . $len . 'f', $number);
        }

        // 整数
        return sprintf('%s', $number);
    }
}

if (!function_exists('decimal_places')) {
    /**
     * 获取小数位数
     *
     * @param $number
     * @return int
     */
    function decimal_places($number): int
    {
        ($len = strlen(strstr($number, '.'))) && $len--;
        return $len;
    }
}

if (!function_exists('hex_reversal')) {
    /**
     * 将16进制字符按字节反转
     * 16进制的字节顺序为高字节在前，低字节在后，如果是低字节在前就需要把字节反转
     *
     * @param string $hex
     * @return string
     */
    function hex_reversal(string $hex): string
    {
        return implode('', array_reverse(str_split($hex, 2)));
    }
}


if (!function_exists('sc_to_num')) {
    /**
     * 科学计数转正常字符串
     *
     * @param mixed $num 科学计数法字符串，如 2.1E-5
     * @param int $double
     * @return mixed|string
     */
    function sc_to_num($num, int $double = 5)
    {
        if (false !== stripos($num, 'e')) {
            $a = explode('e', strtolower($num));

            return bcmul($a[0], bcpow(10, $a[1], $double), $double);
        } else {
            return $num;
        }
    }
}
