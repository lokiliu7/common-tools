<?php

if (!function_exists('now_date_time')) {
    /**
     * 获取当前日期时间
     *
     * @return false|string
     */
    function now_date_time()
    {
        return date('Y-m-d H:i:s');
    }
}

if (!function_exists('print_data')) {
    /**
     * 打印数据后结束
     *
     * @return void
     */
    function print_data(...$data)
    {
        echo '<pre>';
        foreach ($data as $datum) {
            print_r($datum);
            echo '<br>';
        }
        exit();
    }
}

if (!function_exists('password_encrypt')) {
    /**
     * 密码加密
     */
    function password_encrypt($password, $salt): string
    {
        return md5(sha1($password) . $salt);
    }
}

if (!function_exists('get_real_ip')) {
    /**
     * 获取ip地址
     */
    function get_real_ip()
    {
        if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknow')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknow')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknow')) {
            $ip = getenv('REMOTE_ADDR');
        } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknow')) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = 'unknow';
        }

        return $ip;
    }
}

if (!function_exists('arraySearch')) {
    /**
     * 一维数组模糊查询.
     *
     * @param array $arrays 数组
     * @param string $keywords 关键词
     */
    function arraySearch(array $arrays, string $keywords): array
    {
        $arr = [];
        foreach ($arrays as $values) {
            if (stristr($values, $keywords) !== false) {
                $arr[] = $values;
            }
        }

        return $arr;
    }
}

if (!function_exists('object_array')) {
    /**
     * 对象转数组
     *
     * @param $array
     * @return array|mixed
     */
    function object_array($array)
    {
        if (is_object($array)) {
            $array = (array)$array;
        }
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                $array[$key] = object_array($value);
            }
        }

        return $array;
    }
}

if (!function_exists('interval_days')) {
    /**
     * 二个日期之间相差的天数
     *
     * @param $start
     * @param $end
     * @return float
     */
    function interval_days($start, $end): float
    {
        $a_dt = getdate(strtotime($start));
        $b_dt = getdate(strtotime($end));
        $a_new = mktime(12, 0, 0, $a_dt['mon'], $a_dt['mday'], $a_dt['year']);
        $b_new = mktime(12, 0, 0, $b_dt['mon'], $b_dt['mday'], $b_dt['year']);

        return round(abs($a_new - $b_new) / 86400);
    }
}
